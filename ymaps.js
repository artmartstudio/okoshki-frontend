ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.751574, 37.573856],
            zoom: 9
        }, {
            searchControlProvider: 'yandex#search'
        })
    
    ymaps.geolocation.get({
        provider: 'auto'
    }).then(function (result) {
        myMap.setCenter([result.geoObjects.position[0], result.geoObjects.position[1]], 13).then(() => {
            myMap.geoObjects.add(result.geoObjects)    
        })
        
        var circle = new ymaps.geometry.base.Circle([result.geoObjects.position[0], result.geoObjects.position[1]], 0.01)
        console.log(circle.contains([50.447081, 30.517904]))
        console.log(circle.contains([50.449328, 30.518076]))
    });
});