import { assign } from 'lodash/object'

export default (state = {}, action) => {
  switch (action.type) {
    case "YMAP_SET_OBJECT":
      return assign({}, state, {[action.key]: action.value})
    default:
      return state
  }
}
