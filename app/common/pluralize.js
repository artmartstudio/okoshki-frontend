export default function pluralize(count, values) {
  if ([11, 12, 13, 14].indexOf(count % 100) != -1)
    return values[2]
  if (count % 10 == 1)
    return values[0]
  if ([2, 3, 4].indexOf(count % 10) != -1)
    return values[1]
  return values[2]
}
