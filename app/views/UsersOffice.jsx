import React from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import axios from 'axios'
import { authSet } from 'actions/auth'
import { isNull } from 'lodash/lang'
import { Link } from 'react-router'
import momentjs from 'moment'
import pluralize from 'common/pluralize'
import { cloneDeep } from 'lodash/lang'
import { browserHistory } from 'react-router'

import AddObject from 'components/AddObject.jsx'

import 'styles/users-office'

class UsersOffice extends React.Component {
  constructor(props) {
    super(props)

		this.state = {
			popupHidden: true,
			editHidden: true,
      loading: false,
      objects: [],
      areas: undefined,
      currentObject: undefined,
      photoIdx: -1,
      key: 0
		}
  }

  componentDidMount() {
    var token = localStorage.getItem('token')

    if (!isNull(token)) {
      axios.post('/api/isauth', {token: token}).then((resp) => {
        if (resp.data.status === 'authenticated') {
          axios.post('/api/user.get', {token: token}).then((resp) => {
            this.props.dispatch(authSet('userRole', resp.data))
          })
          this.props.dispatch(authSet('isAuth', true))
        }
      })
    }

    axios.get('/api/kiev.areas.get').then(areasresp => {
      var areas = cloneDeep(areasresp.data)
      areas = areas.map((area) => {
        area.title += ' район'
        return area
      })
      areas.unshift({id: -1, title: "Все районы"})
      this.setState({areas})
    })
  }

  showForm() {
		this.setState({
			popupHidden: false
		})
  }

  hideForm() {
	  this.setState({
		  popupHidden: true
	  })
  }

	showEdit() {
		this.setState({
			editHidden: false
		})
  }

  hideEdit() {
	  this.setState({
		  editHidden: true
	  })
  }

  randomPhoto(object) {
    var photo
    [1, 2, 3].every(i => {
      if (object[`photo${i}`]) {
        var file = object[`photo${i}`].file_name;
        photo = `${file}.${file.split('.').pop()}`;
        return false;
      }
    });

    return `/static/uploads/object_photos/original_${photo}`
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user !== undefined) {
      this.setState({loading: true, objects: nextProps.user.objects})
    }
  }

  renderObjects() {
    return this.state.objects.map((object, i) => {
      var delete_via = object.inserted_at
      var expDays = 30 - Math.abs((momentjs(delete_via).diff(momentjs(), 'days') + 1))
      var expDaysHtml = <span className="uo-object-state">Объект будет удалён через <strong>{expDays} {pluralize(expDays, ['день', 'дня', 'дней'])}</strong></span>

      if (expDays <= 0) {
        expDaysHtml = <span className="uo-object-state">Объект удалён</span>
      }

      return (<div className="uo-object" key={i}>
        <img src={this.randomPhoto(object)} />
        <div className="uo-object-content">
          <h3>{object.street}</h3>
          <div className="uo-object-actions">
            <button onClick={() => {
              this.setState({currentObject: object})
              this.showEdit.bind(this)()
            }}>Редактировать</button>
          <button onClick={() => {
              axios.get(`/api/object.update/${object.id}`)
              location.reload()
            }}>Обновить</button>
          <button onClick={() => {
              var possible = confirm('Вы действительно хотите удалить объект?')
              if (possible) {
                axios.get(`/api/object.delete/${object.id}`)
                location.reload()
              }
            }}>Удалить</button>
          </div>
          {expDaysHtml}
        </div>
      </div>)
    })
  }

  render() {
    if (!this.state.loading) {
      return <div></div>
    }

    return (
      <section className="users-office">
					{ !this.state.popupHidden &&
						<AddObject
              areas={this.state.areas}
							close={ this.hideForm.bind(this) } />
					}
					{ !this.state.editHidden &&
						<AddObject
              areas={this.state.areas}
							close={ this.hideEdit.bind(this) }
              object={this.state.currentObject}
							parameters={{
								header: "Редактировать объект"
							}} />
					}
				<div className="uo-header">
					<h2>Личный кабинет</h2>
					<span>
						<a onClick={ this.showForm.bind(this) }>Добавить объект</a>
						<a href="/">На карту</a>
						<a href="#" onClick={() => {
                localStorage.removeItem('token')
                location.href = '/'
              }} className="uo-sign-out">Выйти</a>
					</span>
				</div>
				<div className="uo-content">
          {this.renderObjects.bind(this)()}
				</div>
      </section>
    )
  }
}

export default connect(
  state => ({
    user: state.auth.userRole,
    map: state.map.mapObject,
    auth: state.auth,
  })
)(UsersOffice)
