import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'
import axios from 'axios'

import Dropdown from 'components/Dropdown'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Checkbox from 'components/Checkbox'

import 'styles/add-object'
import close from 'images/close.svg'

var empty = (obj) => Object.keys(obj).length == 0

class AddObject extends React.Component {
  constructor(props) {
    super(props)

		this.content = [
				{title: '1', value: 1},
				{title: '2', value: 2},
				{title: '3', value: 3},
				{title: '4 и более', value: 'more'},
			]

		this.state = {
			files: [undefined, undefined, undefined],
      filesUpload: [],
      isCreate: false,
      objectState: props.object || {}
		}
  }

  handleMissClick(event) {
		event.target.classList.contains('add-object') ? this.props.close() : {}
  }

  selectPhoto(i) {
		this.refs[`file${ i }`].click()
  }

  handleChoose(i, event) {
		if (event.target.value != "") {
      var reader = new FileReader()
      var url = reader.readAsDataURL(event.target.files[0])
      var filesUpload = this.state.filesUpload
      if (this.state.isCreate) {
        filesUpload.push(event.target.files[0])
      } else {
        filesUpload[i] = event.target.files[0]
      }
      this.setState({filesUpload})
      reader.onloadend = (e) => {
	      var a = this.state.files.slice()
	      a[i] = reader.result
        this.setState({
          files: a,
        })
      }
    }
  }

  update() {
    var { objectState } = this.state;
    var phones = [];

    [0, 1, 2].forEach(i => {
      var value = this.refs[`tel${i}`].value.trim();
      if (value.length > 0) {
        phones.push(value);
      }
    });

    objectState.phones = phones;

    var data = new FormData();
    data.append('object', JSON.stringify(objectState));
    this.state.filesUpload.forEach((file, i) => data.append(`file${i}`, file))
    axios.post(`/api/object.update/${objectState.id}`, data);
    location.reload();
  }

  save() {
    var { objectState } = this.state;
    var phones = [];

    [0, 1, 2].forEach(i => {
      var value = this.refs[`tel${i}`].value.trim();
      if (value.length > 0) {
        phones.push(value);
      }
    });

    objectState.phones = phones;
    objectState.user_id = this.props.user.id;

    var data = new FormData();
    data.append('object', JSON.stringify(objectState));
    this.state.filesUpload.forEach((file, i) => data.append(`file${i}`, file))
    axios.post(`/api/object.add`, data);
    location.reload();
  }

  componentDidMount() {
    if (!this.props.parameters) {
      this.setState({isCreate: true})
    }
  }

  render() {
    if (this.props.areas == undefined) {
      return <div></div>
    }

    var { object } = this.props
    var edit = object ? true : false
    !object && (object = {})

    var roomActive = object.room_count < 4 ? object.room_count - 1 : 3
    var area = this.props.areas.find(obj => obj.id == object.area_id)
    var { objectState } = this.state

    return (
      <div className="add-object" onClick={ this.handleMissClick.bind(this) }>
				<div className="ao-container">
					<span className="ao-close" onClick={ this.props.close }></span>
					<h2>
						{ this.props.parameters && this.props.parameters.header }
						{ !this.props.parameters && "Добавить объект" }
					</h2>
					<div className="ao-input-group">
						<label htmlFor="street">Адрес</label>
						<input defaultValue={object.street || ''} type="text" name="street" placeholder="Введите адрес объекта" onChange={(e) => {
              objectState.street = e.target.value
              this.setState({objectState})
            }} />
					</div>
          <div className="ao-filepickers">
            <label>Добавьте фотографии</label>
            {[0, 1, 2].map((num) => {
              var currPhoto = undefined
              if (this.state.files[num]) {
                currPhoto = this.state.files[num]
              } else if (object[`photo${num+1}`] && 'file_name' in object[`photo${num+1}`]) {
                currPhoto = `/static/uploads/object_photos/original_${object[`photo${num+1}`].file_name}.${object[`photo${num+1}`].file_name.split('.').pop()}`
              }

              return (<span key={num}>
                <input
    							type="file"
    							ref={`file${num}`}
    							onChange={this.handleChoose.bind(this, num)}/>

                <div
    							style={currPhoto && {backgroundImage: `url('${currPhoto}')`}}
    							className={classNames({ "ao-file-picked": this.state.files[num] != undefined ? true : false })}
    							onClick={this.selectPhoto.bind(this, num)}
    						></div>
              </span>)
            })}
          </div>
					<div className="ao-half">
						<label>Выберите район</label>
						<Dropdown active={area} onUpdate={(area) => {
              objectState.area_id = area.id
              this.setState({objectState})
            }} options={this.props.areas} label="Выберите район"/>
					</div>
					<div className="ao-half ao-half-second">
						<label>Количество комнат</label>
						<RadioButtonGroup active={roomActive} onChangeState={(i) => {
              objectState.room_count = this.content[i].value == 'more' ? 5 : this.content[i].value
              this.setState({objectState})
            }} content={this.content} />
					</div>
					<div className="ao-half ao-checkboxes">
						<label htmlFor="price">Сдается/продается</label>
						<div className="left_siderbar__checkbox_group">
							<Checkbox onChangeState={(i) => {
                if (i) {
                  objectState.status = 'Сдается'
                  this.setState({objectState})
                }
              }} label="Сдается" active={!empty(object) && object.status.toLowerCase() === 'сдается'} />
							<Checkbox onChangeState={(i) => {
                if (i) {
                  objectState.status = 'Продается'
                  this.setState({objectState})
                }
              }} label="Продается" active={!empty(object) && object.status.toLowerCase() === 'продается'} />
						</div>
					</div>
					<div className="ao-half ao-half-second ao-input-group">
						<label htmlFor="price">Цена (в долларах)</label>
						<input type="text" name="price" placeholder="Введите цену объекта" onChange={(e) => {
              objectState.in_dollars = true
              objectState.price = parseInt(e.target.value)
              this.setState({objectState})
            }} defaultValue={object.price || ''} />
					</div>
					<div className="ao-input-group">
						<label htmlFor="text">Описание</label>
						<textarea name="text" placeholder="Введите описание объекта" onChange={(e) => {
              objectState.description = e.target.value
              this.setState({objectState})
            }} defaultValue={object.description || ''}></textarea>
					</div>
          <div className="ao-input-wrapper">
            {[0, 1, 2].map(num => {
              return (
                <div className="ao-input-group" key={num}>
      						<label htmlFor={`tel${num}`}>Телефон {num+1}</label>
      						<input defaultValue={!empty(object) && object.object_contacts[num] ? object.object_contacts[num].phone : ''} type="tel" ref={`tel${num}`} placeholder="Введите ваш телефон"/>
      					</div>
              )
            })}
          </div>
					<div className="ao-submit">
						<button onClick={edit ? this.update.bind(this) : this.save.bind(this)}>
							{ this.props.parameters && "Редактировать объект" }
							{ !this.props.parameters && "Добавить объект" }
						</button>
					</div>
				</div>
      </div>
    )
  }
}

export default connect(
  state => ({
    user: state.auth.userRole,
  })
)(AddObject)
