import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'

import 'styles/popup'
import close from 'images/close.svg'

export default class Popup extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showPopup: false
    }
  }

  render() {
    return (
      <div>
        <div className={classNames("popup")} onClick={() => this.props.hide()}></div>

        <div className="popup-wrapper" style={{width: this.props.width}}>
          <div className="popup-header" style={{width: this.props.width}}>
            {this.props.label && <div className="popup-label">{this.props.label}</div>}
            <div className="popup-close" onClick={() => this.props.hide()} dangerouslySetInnerHTML={{__html: close}}></div>
          </div>

          {this.props.children}
        </div>
      </div>
    )
  }
}
