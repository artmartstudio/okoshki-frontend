import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'

export default class RadioButtonGroup extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      actives: [],
    }
  }

  onItemClick(i) {
    var { actives } = this.state
    actives = actives.map((el) => el = false)
    actives[i] = true
    this.setState({actives})
    this.props.onChangeState(i)
  }

  componentDidMount() {
    var { actives } = this.state
    var { active } = this.props

    this.props.content.forEach((_, i) => {
      actives.push(active == i ? true : false)
    })

    this.setState({actives})
  }

  render() {
    var { content } = this.props

    return (
      <div className="radio-button-group">
        <ul>
          {content.map((el, i) => {
            return (<li className={classNames({'active': this.state.actives[i]})} key={i} onClick={this.onItemClick.bind(this, i)}>{el.title}</li>)
          })}
        </ul>
      </div>
    )
  }
}
