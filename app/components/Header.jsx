import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'
import Recaptcha from 'react-recaptcha'
import axios from 'axios'
import { authSet } from 'actions/auth'
import { paySet } from 'actions/pay'
import momentjs from 'moment'
import pluralize from 'common/pluralize'
import { isNaN, isNull } from 'lodash/lang'
import { browserHistory } from 'react-router'

import Popup from 'components/Popup'

class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      popupOpen: false,
      popupAuthOpen: false,
      popupRegistrationOpen: false,
      popupForgotPasswordOpen: false,
      captchaVerify: false,
      forgotPasswordLabel: '',
      forgotPasswordError: false,
      email: '',
      password: '',
      expDays: 0,
      aboutPic: undefined,
    }

    this.clearState = {captchaVerify: false, forgotPasswordLabel: ''}
  }

  hidePopup() {
    this.setState(Object.assign({popupOpen: false}, this.clearState))
  }

  hideAuthPopup() {
    this.props.dispatch(authSet('popupAuthOpen', false))
    this.setState(this.clearState)
    // this.setState(Object.assign({popupAuthOpen: false}, this.clearState))
  }

  hideRegistrationPopup() {
    this.props.dispatch(authSet('popupRegistrationOpen', false))
    this.setState(this.clearState)
    // this.setState(Object.assign({popupRegistrationOpen: false}, this.clearState))
  }

  hideForgotPasswordPopup() {
    this.props.dispatch(authSet('popupForgotPasswordOpen', false))
    this.setState(this.clearState)
    // this.setState(Object.assign({popupForgotPasswordOpen: false}, this.clearState))
  }

  verifyCallback(response) {
    if (response !== '' || response !== undefined || response.length !== 0) {
      this.setState({captchaVerify: true})
    }
  }

  authUser(token) {
    axios.post('/api/user.get', {token: token}).then((resp) => {
      this.props.dispatch(authSet('isAuth', true))
      this.props.dispatch(authSet('userRole', resp.data))
      localStorage.setItem('token', token)
    })
  }

  goAuth() {
    axios.post('/api/auth', {email: this.state.email, password: this.state.password}).then((response) => {
      if (response.data.status === 'success') {
        // this.setState({popupAuthOpen: false})
        this.props.dispatch(authSet('popupAuthOpen', false))
        this.authUser.bind(this, response.data.token)()

        if (this.props._auth.registrationPay) {
          axios.get(`/api/generate.liqpay.data?amount=${this.props.pay.days[this.props.pay.currentIndex].amount}&token=${response.data.token}`).then((resp) => {
            var data = resp.data.data
            var signature = resp.data.signature

            this.props.dispatch(paySet('data', data))
            this.props.dispatch(paySet('signature', signature))
            this.props.dispatch(paySet('showPopup', true))
          })
        }
      }
    })
  }

  componentWillReceiveProps(nextProps) {
    if (this.props._auth.userRole !== undefined) {
      var expirationDate = this.props._auth.userRole.expiration_at
      if (!isNull(expirationDate)) {
        var expDays = momentjs(expirationDate).diff(momentjs(), 'days') + 1
        if (this.props._auth.expDays !== expDays)
          this.props.dispatch(authSet('expDays', expDays))
        this.setState({expDays})
      }
    }
  }

  goSignup() {
    if (this.state.captchaVerify) {
      axios.post('/api/signup', {email: this.state.email, password: this.state.password}).then((response) => {
        if (response.data.status === 'success') {
          this.props.dispatch(authSet('popupRegistrationOpen', false))
          // this.setState({popupRegistrationOpen: false})
          this.authUser.bind(this, response.data.token)()

          var email = {
        		to: this.state.email,
        		subject: 'Okoshki.net - добро пожаловать на сайт',
        		body: 'Вы зарегистрированы на портале Okoshki.net'
        	}

          axios.post('/api/email.send', email)

          if (this.props._auth.registrationPay) {
            axios.get(`/api/generate.liqpay.data?amount=${this.props.pay.days[this.props.pay.currentIndex].amount}&token=${response.data.token}`).then((resp) => {
              var data = resp.data.data
              var signature = resp.data.signature

              this.props.dispatch(paySet('data', data))
              this.props.dispatch(paySet('signature', signature))
              this.props.dispatch(paySet('showPopup', true))
            })
          }
        }
      })
    }
  }

  goForgotPassword() {
    axios.post('/api/user.get.email', {email: this.state.email}).then((resp) => {
      var email = {
    		to: this.state.email,
    		subject: 'Okoshki.net - пароль от аккаунта',
    		body: 'Ваш пароль: ' + resp.data.password
    	}

      axios.post('/api/email.send', email).then(() => this.setState({forgotPasswordLabel: 'Пароль отправлен Вам на E-mail', forgotPasswordError: false}))
    }).catch(() => {
      this.setState({forgotPasswordLabel: 'Такой Email не зарегистрирован', forgotPasswordError: true})
    })
  }

  componentDidMount() {
    axios.get('/api/settings.get').then((resp) => {
      this.setState({aboutPic: resp.data.about_pic})
    })
  }

  render() {
    var auth = this.props.auth.isAuth, email, nday = this.state.expDays
    auth && (email = this.props._auth.userRole !== undefined ? this.props._auth.userRole.email : '')

    return (
      <div>
        <div className="left-header">
          <div className="logotype" onClick={() => location.reload()}></div>

          <div className="right-block">
            {auth && (<span className="email">{email}</span>)}
            {!auth &&
            <div>
              <button
                className={classNames({"right-block-not-auth": !auth})}
                onClick={() => {
                  this.props.dispatch(authSet('popupAuthOpen', !this.props._auth.popupAuthOpen))
                  this.props.dispatch(authSet('registrationPay', false))
                }}>Вход / регистрация</button>
              <button
                className={classNames({"right-block-not-auth": !auth})}
                onClick={() => {
                  this.props.dispatch(authSet('popupAuthOpen', !this.props._auth.popupAuthOpen))
                  this.props.dispatch(authSet('registrationPay', false))
                }}>Добавить объект</button>
            </div>
            }

            {auth && (
              <div>
                <button className="inline-block" style={{cursor: 'default'}}>
                  {nday > 0 && `${pluralize(nday, ['Остался', 'Осталось', 'Осталось'])} ${nday} ${pluralize(nday, ['день', 'дня', 'дней'])}`}
                  {(nday <= 0 || isNaN(nday)) && `Подписка закончена`}
                </button>

                <button className="inline-block" onClick={() => browserHistory.push('/users-office')}>
                  Личный кабинет
                </button>
              </div>
            )}
          </div>
        </div>

        {!auth &&
        <div>
          <div className="mobile-login" onClick={() => this.props.dispatch(authSet('popupAuthOpen', !this.props._auth.popupAuthOpen))}></div>
          <div className="mobile-login blue" onClick={() => this.props.dispatch(authSet('popupAuthOpen', !this.props._auth.popupAuthOpen))}></div>
        </div>}

        <button className="right-button" onClick={() => this.setState({popupOpen: !this.state.popupOpen})}>Как здесь всё работает?</button>

        {this.props._auth.popupAuthOpen &&
        <Popup width={300} label="Вход в систему" hide={this.hideAuthPopup.bind(this)}>
          <div className="form">
            <input type="email" placeholder="E-mail" onChange={(e) => this.setState({email: e.target.value})} />
            <input type="password" placeholder="Пароль" onChange={(e) => this.setState({password: e.target.value})} />
            <a onClick={() => {
                this.props.dispatch(authSet('popupAuthOpen', false))
                this.props.dispatch(authSet('popupRegistrationOpen', true))
                // this.setState({popupAuthOpen: false, popupRegistrationOpen: true})
              }} style={{marginBottom: 0}}>Регистрация</a>
              <a onClick={() => {
                this.props.dispatch(authSet('popupAuthOpen', false))
                this.props.dispatch(authSet('popupForgotPasswordOpen', true))
                    // this.setState({popupAuthOpen: false, popupForgotPasswordOpen: true})
                }}>Забыли пароль?</a>
            <button className="active" onClick={this.goAuth.bind(this)}>Войти в систему</button>
          </div>
        </Popup>}

        {this.props._auth.popupRegistrationOpen && <Popup width={300} label="Регистрация" hide={this.hideRegistrationPopup.bind(this)}>
          <div className="form">
            <input type="email" placeholder="Введите e-mail" onChange={(e) => this.setState({email: e.target.value})} />
            <input type="password" placeholder="Введите пароль" onChange={(e) => this.setState({password: e.target.value})} />
            <a onClick={() => {
              this.props.dispatch(authSet('popupAuthOpen', true))
              this.props.dispatch(authSet('popupRegistrationOpen', false))
                // this.setState({popupAuthOpen: true, popupRegistrationOpen: false})
              }}>Вход</a>

            <Recaptcha
              sitekey="6LdHcg8UAAAAAJpkyQNU7IsdxpeD5nXxHb0qypn8"
              render="explicit"
              size="compact"
              verifyCallback={this.verifyCallback.bind(this)}
              onloadCallback={() => {}}
            />

            <button className={classNames({'active': this.state.captchaVerify})} onClick={this.goSignup.bind(this)}>
              {this.props._auth.registrationPay ? `Регистрация и оплата` : `Регистрация`}
            </button>
          </div>
        </Popup>}

        {this.props._auth.popupForgotPasswordOpen && <Popup width={300} label="Восстановление пароля" hide={this.hideForgotPasswordPopup.bind(this)}>
          <div className="form">
            <input type="email" placeholder="Введите e-mail" onChange={(e) => this.setState({email: e.target.value})} />
            {this.state.forgotPasswordLabel.length > 0 && <p style={{color: this.state.forgotPasswordError ? 'red' : 'green'}}>{this.state.forgotPasswordLabel}</p>}
            <button className="active" onClick={this.goForgotPassword.bind(this)}>Восстановить</button>
          </div>
        </Popup>}

        {this.state.popupOpen && <Popup hide={this.hidePopup.bind(this)}>
          {this.state.aboutPic && <img style={{width: '95%'}} draggable="false" src={`/uploads/object_photos/original_${this.state.aboutPic}.jpg`} alt="" />}
        </Popup>}
      </div>
    )
  }
}

export default connect(
  state =>({
    _auth: state.auth,
    pay: state.pay,
  })
)(Header)
