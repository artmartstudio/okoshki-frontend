import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      checked: false,
    }
  }

  componentDidMount() {
    this.props.active && this.setState({checked: true})
  }

  render() {
    var { label, active } = this.props

    return (
      <div className={classNames("checkbox", {"active": this.state.checked})} onClick={() => {
        this.setState({checked: !this.state.checked})
        this.props.onChangeState(!this.state.checked)
      }}>
        {label}
      </div>
    )
  }
}
