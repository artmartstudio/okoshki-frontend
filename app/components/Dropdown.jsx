import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'
import $ from 'jquery'

import EventListener from 'components/EventListener'

import 'styles/dropdown'

export default class Dropdown extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      dropdownActive: false,
      currentActive: {},
    }
  }

  componentDidMount() {
    var { active } = this.props
    active && this.setState({currentActive: active})
  }

  render() {
    var title = [this.state.currentActive.title || this.props.label].join(' ')

    return (
      <div
        ref="dropdown"
        className={classNames("left_siderbar__dropdown", {"active": this.state.dropdownActive})}
        onClick={() => this.setState({dropdownActive: !this.state.dropdownActive})}>

        <EventListener
          target="window"
          type="global"
          onClick={(e) => {
            ($(this.refs.dropdown).has(e.target).length === 0) &&
              this.setState({dropdownActive: false})
          }} />

        <span className="left_siderbar__dropdown__right__arrow" ref="rightarrow"></span>
        <div className={classNames("left_siderbar__dropdown__content")}>
          <object>{title}</object>
          {this.props.options.map((el, i) => {
            return <span key={i} onClick={() => {
              this.setState({currentActive: this.props.options[i]})
              this.props.onUpdate(this.props.options[i])
            }}>{el.title}</span>
          })}
        </div>
      </div>
    )
  }
}
