export function paySet(key, value) {
  return (dispatch, getState) => {
    dispatch({type: "PAY_SET", key, value})
  }
}
